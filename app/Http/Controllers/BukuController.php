<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Buku;


class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $crud_buku = Buku::paginate(4);
        return view('buku.index', ['crud_buku' => $crud_buku]);
    }


    public function delete($id)
    {
        DB::table('crud_buku')->where('id', $id)->delete();
        return redirect('/buku');

        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('buku.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama'=>'required|nama|unique:crud_buku',
            'penerbit'=>'required',
            'stok'=>'required|numeric',
            'kategori'=>'required',
        ]); 

        DB::table('crud_buku')->insert([
            'nama' => $request->nama,
            'penerbit' => $request->penerbit,
            'stok' => $request->stok,
            'kategori' => $request->kategori
        ]);

        return redirect('/buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $crud_buku= DB::table('crud_buku')->where('id',$id)->get();
        return view('buku.edit', ['crud_buku' => $crud_buku]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('crud_buku')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'penerbit' => $request->penerbit,
            'stok' => $request->stok,
            'kategori' => $request->kategori
        ]);

        return redirect('/buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
