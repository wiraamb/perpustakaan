<!DOCTYPE html>
<html>
<body>

	<h3>Edit Daftar Buku</h3>
 
	<a href="/buku"> Kembali</a>
	
	<br/>
	<br/>
 
	@foreach($crud_buku as $buku)
	<form action="/buku/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $buku->id }}"> <br/>
		Nama Buku <input type="text" required="required" name="nama" value="{{ $buku->nama }}"> <br/> <br>
		Penerbit <input type="text" required="required" name="penerbit" value="{{ $buku->penerbit }}"> <br/> <br>
		Stok <input type="number" required="required" name="stok" value="{{ $buku->stok }}"> <br/> <br>
        Kategori <input type="text" required="required" name="kategori" value="{{ $buku->kategori }}"> <br/> <br>
		
		<input type="submit" value="Simpan Data">
	</form>
	@endforeach
		
</body>
</html>