@extends('layout.main')

@section('title', 'Perpustakaan')

  
@section('container')

<style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style>

    <div class="container"> 
    <div class="row">
    <div class="col-10"> 
    <h1 class="mt-3">Daftar Buku</h1>

    <a href="/Buku/create"> +Add Buku Baru </a>
  <br>

    <table class="table">
    <thead class="thead-dark">
        <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Buku</th>
        <th scope="col">Penerbit</th>
        <th scope="col">Stok</th>
        <th scope="col">Kategori</th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    
    @foreach ($crud_buku as $buku)
    <tbody>
    <tr>

        <th scope="row">{{ $loop->iteration }}</th>
        <td>{{ $buku->nama }}</td>
        <td>{{ $buku->penerbit }}</td>
        <td>{{ $buku->stok }}</td>
        <td>{{ $buku->kategori}}</td>
        <td>
            <a href="/buku/edit/{{ $buku->id }}" class="badge badge-success">edit</a>
            <a onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"href="/buku/delete/{{ $buku->id }}" class="badge badge-danger">delete</a>

        </td>
    </tr>
   @endforeach
    </tbody>
    </table>

    {{ $crud_buku->links()}} 
    </div>
    </div>
    </div>
@endsection
 