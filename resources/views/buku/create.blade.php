<!DOCTYPE html>
<html>
<body>
	<h3>Data Buku</h3>
 
	<a href="/buku"> Kembali</a>
	
	<br/>
	<br/>
 
	{{-- menampilkan error validasi --}}
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
 
                            <br/>
                             <!-- form validasi -->
                            <form action="/buku/store" method="post">
                                {{ csrf_field() }}
 
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input class="form-control" type="text" name="nama" value="{{ old('nama') }}"> 

                                </div> <br>
                                <div class="form-group">
                                    <label for="penerbit">Penerbit</label>
                                    <input class="form-control" type="text" name="penerbit" value="{{ old('penerbit') }}"> <br>
                                </div><br>

                                <div class="form-group">
                                    <label for="stok">Stok</label>
                                    <input class="form-control" type="text" name="stok" value="{{ old('stok') }}"> <br>
                                </div><br>

								<div class="form-group">
                                    <label for="penerbit">kategori</label>
                                    <input class="form-control" type="text" name="kategori" value="{{ old('kategori') }}"> <br>
                                </div><br>

                                <div class="form-group">
                                    <input class="btn btn-primary" type="submit" value="simpan data"> <br>
                                </div>
                            </form>
 
 
</body>
</html>