<?php

Route::get('/', 'PagesController@home');
Route::get('/about', 'PagesController@about');

Route::get('/buku', 'BukuController@index');

Route::get('/Buku/create', 'BukuController@create');
Route::post('/buku/store', 'BukuController@store');

Route::get('/buku/delete/{id}', 'BukuController@delete');

Route::get('/buku/edit/{id}', 'Bukucontroller@edit');
Route::post('/buku/update', 'BukuController@update');
